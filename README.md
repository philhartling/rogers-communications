Experienced people leader, with over twenty five years experience in the telecommunications industry. Over this time I've led strategic, marketing, sales, customer service and operations functions. I am passionate about serving customers, developing my people and winning in the market.

Website: http://www.rogers.com
